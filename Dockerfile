FROM ubuntu:22.04

RUN apt-get update && apt-get install -y --no-install-recommends build-essential libproj-dev libpng-dev wget libsqlite3-dev

RUN wget --content-disposition --no-check-certificate https://github.com/OSGeo/gdal/releases/download/v3.0.4/gdal-3.0.4.tar.gz && tar -zxvf gdal-3.0.4.tar.gz

WORKDIR /gdal-3.0.4

RUN ./configure --disable-all-optional-drivers --enable-driver-gpkg
RUN make -j$(nproc) && make install

WORKDIR /
RUN rm -rf /gdal-3.0.4

ADD efal /opt/efal

ENV EFAL_SDK_DIR=/opt/efal
ENV LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$EFAL_SDK_DIR
ENV GDAL_DRIVER_PATH=$GDAL_DRIVER_PATH:/opt/efal

RUN ldconfig

RUN ogrinfo --formats | grep EFAL

